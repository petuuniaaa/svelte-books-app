# Svelte-books-app

Pré-requis :    
- Avoir internet 
- Avoir NodeJS d'installé 

Pour installer le projet : 
- git clone https://gitlab.com/petuuniaaa/svelte-books-app.git
- Se déplacer dans le dossier "svelte-books-app"
- npm install 
- node_modules/pouchdb-server/bin/pouchdb-server --port 5555
- npm run dev 